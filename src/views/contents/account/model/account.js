export const AccountModel = {
  accountParam(text, obj, page) {
    return {
      Page: text == undefined ? page : (page = 1),
      FirstName: obj.firstName,
      LastName: obj.lastName,
      Status:
        obj.status == undefined || obj.status == "" ? undefined : obj.status,
    };
  },
};

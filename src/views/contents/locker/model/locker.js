export const LockerModel = {
  lockerParam(text, obj, page, perPage, role) {
    if (role == "Admin") {
      return {
        page: text == undefined ? page : (page = 1),
        perPage: perPage,
        FirstName: obj.firstName,
        LastName: obj.lastName,
        FormCode: obj.formCode,
        LockerCode: obj.lockerCode,
        LocateName: obj.locateName,
        SubDistrict: obj.subDistrict,
        PostalCode: obj.postalCode,
        District: obj.district,
        Province: obj.province,
        Status:
          obj.status == undefined || obj.status == "" ? undefined : obj.status,
      };
    } else {
      return {
        page: text == undefined ? page : (page = 1),
        perPage: perPage,
        accountId: localStorage.getItem("accountId"),
        FirstName: obj.firstName,
        LastName: obj.lastName,
        FormCode: obj.formCode,
        LockerCode: obj.lockerCode,
        LocateName: obj.locateName,
        SubDistrict: obj.subDistrict,
        PostalCode: obj.postalCode,
        District: obj.district,
        Province: obj.province,
        Status:
          obj.status == undefined || obj.status == "" ? undefined : obj.status,
      };
    }
  },
};

export const LocationModel = {
  locationParam(text, obj, page, perPage, role) {
    if (role == "Admin") {
      return {
        page: text == undefined ? page : (page = 1),
        perPage: perPage,
        FirstName: obj.firstName,
        LastName: obj.lastName,
        District: obj.district,
        Province: obj.province,
        LocateName: obj.locateName,
        SubDistrict: obj.subDistrict,
        PostalCode: obj.postalCode,
        Status:
          obj.status == undefined || obj.status == "" ? undefined : obj.status,
      };
    } else {
      return {
        page: text == undefined ? page : (page = 1),
        perPage: perPage,
        FirstName: obj.firstName,
        LastName: obj.lastName,
        District: obj.district,
        Province: obj.province,
        LocateName: obj.locateName,
        SubDistrict: obj.subDistrict,
        accountId: localStorage.getItem("accountId"),
        PostalCode: obj.postalCode,
        Status:
          obj.status == undefined || obj.status == "" ? undefined : obj.status,
      };
    }
  },
  locationEdit(obj) {
    return {
      LocateId: obj.locateId,
      LocateName: obj.locateName,
      Latitude: obj.latitude,
      Longtitude: obj.longtitude,
      SubDistrict: obj.subDistrict,
      District: obj.district,
      PostalCode: obj.postalCode,
      Province: obj.province,
      accountId: localStorage.getItem("accountId"),
    };
  },
  locationApprove(status, obj, remark) {
    if (status == "approve") {
      return {
        LocateId: obj.locateId,
        Status: status,
      };
    } else {
      return {
        LocateId: obj.locateId,
        Status: status,
        Remark: remark,
      };
    }
  },
};

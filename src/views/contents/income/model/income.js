import moment from "moment";

export const IncomenModel = {
  incomeLocation(obj, page, perPage, text) {
    return {
      LocateName: obj.locateName,
      FirstName: obj.firstName,
      LastName: obj.lastName,
      SubDistrict: obj.subDistrict,
      District: obj.dstrict,
      Province: obj.province,
      PostalCode: obj.postalCode,
      AccountId: localStorage.getItem("accountId"),
      page: text == undefined ? page : (page = 1),
      perPage: perPage,
      DurationType:
        localStorage.getItem("type") == null
          ? obj.durationType
          : localStorage.getItem("type"),
      StartDate:
        obj.StartDate == undefined
          ? localStorage.getItem("start") == null
            ? moment(new Date(obj.dates[1]))
                .add(-543, "year")
                .format("YYYY-MM-DD")
            : moment(new Date(localStorage.getItem("start")))
                .add(-543, "year")
                .format("YYYY-MM-DD")
          : moment(obj.StartDate)
              .add(-543, "year")
              .format("YYYY-MM-DD"),
      EndDate:
        obj.EndDate == undefined
          ? localStorage.getItem("end") == null
            ? moment(new Date(obj.dates[0]))
                .add(-543, "year")
                .format("YYYY-MM-DD")
            : moment(new Date(localStorage.getItem("end")))
                .add(-543, "year")
                .format("YYYY-MM-DD")
          : moment(obj.EndDate)
              .add(-543, "year")
              .format("YYYY-MM-DD"),
    };
  },
  incomeLocker(obj, page, perPage, text, paramObj) {
    return {
      LockerCode: obj.lockerCode,
      AccountId: localStorage.getItem("accountId"),
      page: text == undefined ? page : (page = 1),
      perPage: perPage,
      DurationType:
        localStorage.getItem("type") == null
          ? obj.durationType
          : localStorage.getItem("type"),
      StartDate:
        obj.StartDate == undefined
          ? localStorage.getItem("start") == null
            ? moment(new Date(obj.dates[1]))
                .add(-543, "year")
                .format("YYYY-MM-DD")
            : moment(new Date(localStorage.getItem("start")))
                .add(-543, "year")
                .format("YYYY-MM-DD")
          : moment(obj.StartDate)
              .add(-543, "year")
              .format("YYYY-MM-DD"),
      EndDate:
        obj.EndDate == undefined
          ? localStorage.getItem("end") == null
            ? moment(new Date(obj.dates[0]))
                .add(-543, "year")
                .format("YYYY-MM-DD")
            : moment(new Date(localStorage.getItem("end")))
                .add(-543, "year")
                .format("YYYY-MM-DD")
          : moment(obj.EndDate)
              .add(-543, "year")
              .format("YYYY-MM-DD"),
      LocateId: paramObj.locateId,
    };
  },
  incomeLockerRoom(obj, page, perPage, text, paramObj) {
    return {
      AccountId: localStorage.getItem("accountId"),
      page: text == undefined ? page : (page = 1),
      perPage: perPage,
      DurationType:
        localStorage.getItem("type") == null
          ? obj.durationType
          : localStorage.getItem("type"),
      StartDate:
        obj.StartDate == undefined
          ? localStorage.getItem("start") == null
            ? moment(new Date(obj.dates[1]))
                .add(-543, "year")
                .format("YYYY-MM-DD")
            : moment(new Date(localStorage.getItem("start")))
                .add(-543, "year")
                .format("YYYY-MM-DD")
          : moment(obj.StartDate)
              .add(-543, "year")
              .format("YYYY-MM-DD"),
      EndDate:
        obj.EndDate == undefined
          ? localStorage.getItem("end") == null
            ? moment(new Date(obj.dates[0]))
                .add(-543, "year")
                .format("YYYY-MM-DD")
            : moment(new Date(localStorage.getItem("end")))
                .add(-543, "year")
                .format("YYYY-MM-DD")
          : moment(obj.EndDate)
              .add(-543, "year")
              .format("YYYY-MM-DD"),
      LockerId: paramObj.lockerId,
    };
  },
};

export const BASEURL = "http://139.59.229.66/api/";
// export const BASEURL = "https://webservice-locker.ml/api/";
// export const BASEURL = "https://localhost:5001/api/";
const token = JSON.parse(localStorage.getItem("token"));
export const HEADER_BEARER = {
  headers: {
    Authorization: `Bearer ${token == null ? null : token.value}`,
  },
};

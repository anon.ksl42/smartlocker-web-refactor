export const GoogleService = {
  googleResult(results, selectedItem) {
    var lat = results[0].geometry.location.lat();
    var lng = results[0].geometry.location.lng();
    let addressData = {
      LocateName: selectedItem == undefined ? "" : selectedItem.name,
      Province: "",
      PostalCode: "",
      District: "",
      SubDistrict: "",
    };
    for (let i in results[0].address_components) {
      if (results[0].address_components[i].types[0] == "postal_code")
        addressData.PostalCode = results[0].address_components[i].short_name;
      if (
        results[0].address_components[i].types[0] ==
        "administrative_area_level_1"
      )
        addressData.Province = results[0].address_components[i].short_name;
      if (
        results[0].address_components[i].types[0] == "locality" ||
        results[0].address_components[i].types[2] == "sublocality_level_2"
      )
        addressData.SubDistrict = results[0].address_components[i].short_name;
      if (
        results[0].address_components[i].types[0] ==
          "administrative_area_level_2" ||
        results[0].address_components[i].types[2] == "sublocality_level_1"
      )
        addressData.District = results[0].address_components[i].short_name;
    }
    let res = {
      locateName: addressData.LocateName,
      latitude: lat,
      longtitude: lng,
      subDistrict: addressData.SubDistrict,
      district: addressData.District,
      postalCode: addressData.PostalCode,
      province: addressData.Province,
    };
    return res;
  },
};

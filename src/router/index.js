import { index } from "./combine";

export const routes = [
  {
    path: "/",
    name: "login",
    component: index.Login,
  },
  {
    path: "/register-partner",
    name: "register",
    component: index.Register,
  },
  {
    path: "/register-message",
    name: "register-message",
    component: index.RegisterMessage,
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: index.Dashboard,
    meta: { authorize: ["Partner", "Admin"] },
  },
  {
    path: "/location-list",
    name: "location-list",
    component: index.ListLocation,
    meta: { authorize: ["Admin", "Partner"] },
  },
  {
    path: "/locker-list",
    name: "locker-list",
    component: index.ListLocker,
    meta: { authorize: ["Admin", "Partner"] },
  },
  {
    path: "/location-request",
    name: "location-request",
    component: index.LocationRequest,
    meta: { authorize: ["Partner"] },
  },
  {
    path: "/locker-request",
    name: "locker-request",
    component: index.LockerRequest,
    meta: { authorize: ["Partner"] },
  },
  {
    path: "/income-location",
    name: "income-location",
    component: index.IncomeLocation,
    meta: { authorize: ["Partner", "Admin"] },
  },
  {
    path: "/income-locker",
    name: "income-locker",
    component: index.IncomeLocker,
    meta: { authorize: ["Partner", "Admin"] },
  },
  {
    path: "/create-repairform",
    name: "createRepairForm",
    component: index.AddRepairLocker,
    meta: { authorize: ["Partner"] },
  },
  {
    path: "/repair",
    name: "repair",
    component: index.ListRepair,
    meta: { authorize: ["Partner", "Admin"] },
  },
  {
    path: "/income-lockerroom",
    name: "income-lockerroom",
    component: index.IncomeLockerRoom,
    meta: { authorize: ["Partner", "Admin"] },
  },
  {
    path: "/verify-applicants",
    name: "verify-applicants",
    component: index.ListAccount,
    meta: { authorize: ["Admin"] },
  },
  { path: "*", redirect: "/" },
];

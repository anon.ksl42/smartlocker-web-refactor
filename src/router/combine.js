import Login from "../views/contents/account/Login.vue";
import Dashboard from "../views/contents/dashboard/Dashboard.vue";
import Register from "../views/contents/account/Register.vue";
import RegisterMessage from "../views/contents/account/RegisterMessage.vue";
import ListAccount from "../views/contents/account/ListAccount.vue";
import ListLocation from "../views/contents/location/ListLocation.vue";
import LocationRequest from "../views/contents/location/LocationRequest.vue";
import LockerRequest from "../views/contents/locker/LockerRequest.vue";
import ListLocker from "../views/contents/locker/ListLocker.vue";
import IncomeLocation from "../views/contents/income/IncomeLocation.vue";
import IncomeLocker from "../views/contents/income/IncomeLocker.vue";
import IncomeLockerRoom from "../views/contents/income/IncomeLockerRoom.vue";
import AddRepairLocker from "../views/contents/repair/AddRepairLocker.vue";
import ListRepair from "../views/contents/repair/ListRepair.vue";

export const index = {
  Login: Login,
  Dashboard: Dashboard,
  Register: Register,
  RegisterMessage: RegisterMessage,
  ListAccount: ListAccount,
  ListLocation: ListLocation,
  LocationRequest: LocationRequest,
  LockerRequest: LockerRequest,
  ListLocker: ListLocker,
  IncomeLocation: IncomeLocation,
  IncomeLocker: IncomeLocker,
  IncomeLockerRoom: IncomeLockerRoom,
  AddRepairLocker: AddRepairLocker,
  ListRepair: ListRepair,
};

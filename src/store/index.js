import { AuthStore } from "./auth_store";
import { SnackBarStore } from "./snackbar_store";
import { DashboardStore } from "./dashboard_store";
import { LocationStore } from "./location_store";
import { LockerRequestStore } from "./locker_request_store";
import { IncomeStore } from "./income_store";
import { RepairFormStore } from "./repair_form_Store";


export default {
  modules: {
    auth: AuthStore,
    snackbar: SnackBarStore,
    dashboard: DashboardStore,
    locate: LocationStore,
    locker: LockerRequestStore,
    income: IncomeStore,
    repair: RepairFormStore,
  },
};

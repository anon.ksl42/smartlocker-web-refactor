import Dashboard from "../services/dashboard_service";
export const DashboardStore = {
  namespaced: true,
  state: {
    bookingLocate: [],
    Income: {},
    LocateAndLocationCount: [],
    bookingLocker: [],
    error: false,
    loading: true,
  },
  mutations: {
    SET_BOOKING_LOCATE(state, payload) {
      state.bookingLocate = payload;
    },
    SET_BOOKING_LOCKER(state, payload) {
      state.bookingLocker = payload;
    },
    SET_INCOME(state, payload) {
      state.Income = payload;
    },
    SET_LOCATE_LOCATION_COUNT(state, payload) {
      state.LocateAndLocationCount = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
  },
  getters: {
    bookingLocation(state) {
      return state.bookingLocate;
    },
    bookingLocker(state) {
      return state.bookingLocker;
    },
    income(state) {
      return state.Income;
    },
    locateAndLocation(state) {
      return state.LocateAndLocationCount;
    },
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
  },
  actions: {
    async getLocateAndLocationCount({ commit }) {
      let res = await Dashboard.getLocateAndLocationCount();

      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
        commit("SET_LOADING", false);
        commit("SET_LOCATE_LOCATION_COUNT", res.data.content);
      }
    },
    async getIncome({ commit }, rangeGraphType) {
      let res = await Dashboard.getIncome(rangeGraphType);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
        commit("SET_LOADING", false);
        commit("SET_INCOME", res.data.content);
      }
    },
    async getBookingLocate({ commit }, data) {
      let res = await Dashboard.getBookingLocate(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
        commit("SET_BOOKING_LOCATE", res.data.content);
      }
    },
    async getBookingLocker({ commit }, data) {
      let res = await Dashboard.getBookingLocker(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
        commit("SET_BOOKING_LOCKER", res.data.content);
      }
    },
  },
};

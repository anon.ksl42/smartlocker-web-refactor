import Location from "../services/location_service";

export const LocationStore = {
  namespaced: true,
  state: {
    result: [],
    error: false,
    loading: true,
  },
  mutations: {
    SET_RESULT(state, payload) {
      state.result = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
  },
  getters: {
    result(state) {
      return state.result;
    },
    error(state) {
      return state.error;
    },
    loading(state) {
      return state.loading;
    },
  },
  actions: {
    async getLocationByAccountId({ commit }) {
      let accountId = localStorage.getItem("accountId");
      let res = await Location.getAllLocationById(accountId);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { content } = res.data;
        commit("SET_RESULT", content);
        commit("SET_ERROR", false);
      }
    },
    async editLocation({ commit }, data) {
      let res = await Location.editLocation(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
    },

    async addLocation({ commit }, data) {
      let res = await Location.addLocation(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_RESULT", res.data.content);
        commit("SET_ERROR", false);
      }
    },
    async getLocationAll({ commit }, data) {
      let res = await Location.getAllLocation(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;
        commit("SET_RESULT", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async approveLocation({ commit }, data) {
      let res = await Location.approveLocation(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
    },
  },
};

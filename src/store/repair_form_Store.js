import RepairForm from "../services/repair_service";

export const RepairFormStore = {
  namespaced: true,
  state: {
    data: [],
    repairLocker: {},
    LkRoomIdList: [],
    loading: true,
    error: false,
  },
  mutations: {
    SET_DATA(state, payload) {
      state.data = payload;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_LK_LOOM_ID(state, payload) {
      state.LkRoomIdList = payload;
    },
    SET_REPAIR_LOCKER(state, payload) {
      state.repairLocker = payload;
    },
  },
  getters: {
    data(state) {
      return state.data;
    },
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
    lkRoomIdList(state) {
      return state.LkRoomIdList;
    },
    repairLocker(state) {
      return state.repairLocker;
    },
  },
  actions: {
    async getSummaryRepair({commit},accountId){
      let result = await RepairForm.getSummaryRepair(accountId);
      if (result == 400 || result == 404 || result == 409 || result == 500)
        commit("SET_ERROR", true);
      else {
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
      commit("SET_DATA", result.data.content);
    },
    async getAllLockerRoomByLockerId({ commit }, lockerId) {
      let result = await RepairForm.getAllLockerRoom(lockerId);
      if (result == 400 || result == 404 || result == 409 || result == 500)
        commit("SET_ERROR", true);
      else {
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
      commit("SET_DATA", result.data.content);
    },
    async approve({ commit }, data) {
      let result = await RepairForm.approveRepairForm(data);
      if (result == 400 || result == 404 || result == 409 || result == 500)
        commit("SET_ERROR", true);
      else {
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async approveFinish({ commit }, data) {
      let result = await RepairForm.finishRepairForm(data);
      if (result == 400 || result == 404 || result == 409 || result == 500)
        commit("SET_ERROR", true);
      else {
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async getAllRepair({ commit }, data) {
      let result = await RepairForm.getRepairForm(data);
      if (result == 400 || result == 404 || result == 409 || result == 500)
        commit("SET_ERROR", true);
      else {
        const { content, totalElement, perPage } = result.data;
        let length = Math.ceil(totalElement / perPage);
        commit("SET_DATA", { content, length });
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    ChangeLkRoomIdList({ commit }, data) {
      commit("SET_LK_LOOM_ID", data);
    },
    initialLockerToRepair({ commit }, data) {
      commit("SET_REPAIR_LOCKER", data);
    },
    async createReportForm({ commit }, data) {
      commit("SET_LOADING", true);
      let result = await RepairForm.addRepairForm(data);
      if (result == 400 || result == 404 || result == 409 || result == 500) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
      commit("SET_LOADING", false);
      commit("SET_DATA", result.data.content);
    },
    clearState({ commit }) {
      commit("SET_ERROR", false);
      commit("SET_LOADING", true);
      commit("SET_DATA", []);
      commit("SET_LK_LOOM_ID", []);
      commit("SET_REPAIR_LOCKER", {});
    },
  },
};

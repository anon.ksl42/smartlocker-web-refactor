//import { getLocalUser } from "../helpers/auth";
import Income from "../services/income_service";

export const IncomeStore = {
  namespaced: true,
  state: {
    result: [],
    urlExport: "",
    error: false,
    loading: true,
  },
  mutations: {
    SET_RESULT(state, payload) {
      state.result = payload;
    },
    SET_URLEXPORT(state, payload) {
      state.urlExport = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
  },
  getters: {
    result(state) {
      return state.result;
    },
    urlExport(state) {
      return state.urlExport;
    },
    error(state) {
      return state.error;
    },
    loading(state) {
      return state.loading;
    },
  },
  actions: {
    async exportExcelLocation({ commit }, data) {
      let res = await Income.exportExcelLocation(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_URLEXPORT", res.data);
        commit("SET_ERROR", false);
      }
    },
    async exportExcelLocker({ commit }, data) {
      let res = await Income.getIncomeExcelLocker(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_URLEXPORT", res.data);
        commit("SET_ERROR", false);
      }
    },
    async getIncomeExcelLockerRoom({ commit }, data) {
      let res = await Income.getIncomeExcelLockerRoom(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_URLEXPORT", res.data);
        commit("SET_ERROR", false);
      }
    },
    async getIncome({ commit }, data) {
      let res = await Income.getIncome(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;
        commit("SET_RESULT", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async getIncomeLocker({ commit }, data) {
      let res = await Income.getIncomeLocker(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;
        commit("SET_RESULT", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async getIncomeLockerRoom({ commit }, data) {
      let res = await Income.getIncomeLockerRoom(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;
        commit("SET_RESULT", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
  },
};

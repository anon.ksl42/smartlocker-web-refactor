import { getLocalUser } from "../util/auth";
import auth from "../services/auth_service";
import jwt_decode from "jwt-decode";
import { TokenExpiry } from "../util/token_expiry";
const roleUser = getLocalUser();
export const AuthStore = {
  namespaced: true,
  state: {
    roleUser: roleUser,
    result: null,
    loading: true,
    error: false,
  },
  mutations: {
    SET_LOGIN(state, payload) {
      const { role, fullName, token, accountId } = payload;
      state.roleUser = role;
      // alert(JSON.stringify(payload))
      localStorage.setItem("user", JSON.stringify(state.roleUser));
      localStorage.setItem("fullName", fullName);
      // localStorage.setItem("token", token);
      TokenExpiry.setWithExpiry("token", token, 1);
      localStorage.setItem("accountId", accountId);
    },
    SET_LOGOUT(state) {
      localStorage.removeItem("user");
      localStorage.removeItem("fullName");
      localStorage.removeItem("token");
      localStorage.removeItem("accountId");
      localStorage.removeItem("type");
      localStorage.removeItem("start");
      localStorage.removeItem("end");
      state.roleUser = null;
    },
    SET_RESULT(state, payload) {
      state.result = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
  },
  getters: {
    roleUser(state) {
      return state.roleUser;
    },
    result(state) {
      return state.result;
    },
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
  },
  actions: {
    async login({ commit }, data) {
      let res = await auth.authentication(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { token, firstName, lastName, accountId } = res.data.content;
        let decoded = await jwt_decode(token);
        let payload = {
          fullName: `${firstName} ${lastName}`,
          role: decoded.role,
          token: token,
          accountId: accountId,
        };
        commit("SET_LOGIN", payload);
        commit("SET_LOADING", false);
      }
    },
    async register({ commit }, data) {
      let res = await auth.register(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
        commit("SET_RESULT", true);
      }
    },
    logout({ commit }) {
      commit("SET_LOGOUT");
    },
    async listAccount({ commit }, data) {
      let res = await auth.listAccount(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;

        commit("SET_RESULT", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async approveAccount({ commit }, data) {
      let res = await auth.approveAccount(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_RESULT", true);
        commit("SET_ERROR", false);
      }
    },
    async validDupEmail({ commit }, data) {
      let res = await auth.dupEmail(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else if (res.status == 204) {
        commit("SET_RESULT", false);
      } else {
        commit("SET_RESULT", true);
        commit("SET_ERROR", false);
      }
    },
  },
};

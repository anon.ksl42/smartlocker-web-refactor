import moment from "moment";

export const PublicFunction = {
  fomatDate(item) {
    let newDate = moment(new Date(item))
      .add(543, "year")
      .locale("th")
      .format("LL");
    return newDate;
  },
  dateNow() {
    let start = Date.now();
    let today = new Date(start).toISOString().split("T")[0];
    return today;
  },
  fullName(item) {
    let fullName = `${item.firstName} ${item.lastName}`;
    return fullName;
  },
  shortName(name) {
    let res = name.slice(0, 3);
    if (res === "นาย" || res === "นาง") return name.slice(3, 4);
    else return name.slice(0, 1);
  },
  status(item) {
    if (item.status === "VF") return "ตรวจสอบฟอร์ม";
    else if (item.status === "RF") return "แก้ไขฟอร์ม";
    else if (item.status === "WD") return "รอการสร้าง diagram";
    else if (item.status === "VD") return "ตรวจสอบ diagram";
    else if (item.status === "RD") return "แก้ไข diagram";
    else if (item.status === "WP") return "รอการชำระเงิน";
    else if (item.status === "VP") return "ตรวจสอบหลักฐานการชำระเงิน";
    else if (item.status === "RP") return "หลักฐานการชำระเงินไม่ผ่าน";
    else if (item.status === "WA") return "รอการติดตั้งล็อคเกอร์";
    else if (item.status === "A") return "ติดตั้งล็อคเกอร์แล้ว";
    else if (item.status === "I") return "ไม่ได้ใช้งาน";
    else if (item.status === "R") return "ไม่อนุมัติการสร้างล็อคเกอร์";
  },
  statusLocation(item) {
    if (item.status === "W") return "กำลังตรวจสอบ";
    if (item.status === "WE") return "รอการแก้ไขข้อมูล";
    else if (item.status === "R") return "ไม่อนุมัติพื้นที่";
    else if (item.status === "A") return "อนุมัติพื้นที่";
  },
  statusAccount(item) {
    if (item.status === "W") return "รอตรวจสอบ";
    if (item.status === "WV") return "รอการยืนยันตัวตนจาก Partner";
    else if (item.status === "A") return "อนุมัติเรียบร้อย";
    else if (item.status === "R") return "ไม่ผ่านการอนุมัติ";
  },
  colorStatus(item) {
    if (item.status === "RP" || item.status === "R") {
      return "red";
    } else if (item.status === "A") {
      return "green";
    } else {
      return "orange";
    }
  },
  statusRepair(item) {
    if (item.status === "VF" || item.status == "W") return "รอตรวจสอบ";
    if (item.status === "P") return "ดำเนินการซ่อม";
    else if (item.status === "R") return "ไม่ผ่านการอนุมัติ";
    else if (item.status === "A") return "ซ่อมเสร็จสิ้น";
  },
  colorStatusRepair(item) {
    if (item.status === "R") {
      return "red";
    } else if (item.status === "A") {
      return "green";
    } else {
      return "orange";
    }
  },
};

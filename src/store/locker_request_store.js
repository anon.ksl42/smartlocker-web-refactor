import LockerForm from "../services/locker_request_service";

export const LockerRequestStore = {
  namespaced: true,
  state: {
    error: false,
    formList: [],
    loading: true,
    totalPages: null,
    lockerSize: [],
    lockerDiagram: [],
  },
  mutations: {
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_FORM_LIST(state, payload) {
      state.formList = payload;
    },
    SET_LOCKERSIZE(state, payload) {
      state.lockerSize = payload;
    },
    SET_LOCKERDIAGRAM(state, payload) {
      state.lockerDiagram = payload;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
  },
  getters: {
    error(state) {
      return state.error;
    },
    lockerSize(state) {
      return state.lockerSize;
    },
    lockerDiagram(state) {
      return state.lockerDiagram;
    },
    formList(state) {
      return state.formList;
    },
    loading(state) {
      return state.loading;
    },
  },
  actions: {
    async getAllFormRequestByAccountId({ commit }, data) {
      let res = await LockerForm.getAllFormRequestByAccountId(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;
        commit("SET_FORM_LIST", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async getAllFormAdmin({ commit }, data) {
      let res = await LockerForm.getFiller(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { totalElement, perPage } = res.data;
        let length = Math.ceil(totalElement / perPage);
        res.data.length = length;
        commit("SET_FORM_LIST", res.data);
        commit("SET_LOADING", false);
        commit("SET_ERROR", false);
      }
    },
    async addLockerRequest({ commit }, data) {
      let res = await LockerForm.addRequestForm(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
    },
    async approveForm({ commit }, data) {
      let res = await LockerForm.approveForm(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
    },
    async approveFormandInstall({ commit }, data) {
      let res = await LockerForm.approveFormandInstall(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
    },
    async getLockerSize({ commit }) {
      let res = await LockerForm.getSize();
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { content } = res.data;
        commit("SET_ERROR", false);
        commit("SET_LOCKERSIZE", content);
      }
    },
    async createDiagram({ commit }, data) {
      let res = await LockerForm.createDiagram(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        commit("SET_ERROR", false);
      }
    },
    async getDiagram({ commit }, data) {
      let res = await LockerForm.getDiagram(data);
      if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        commit("SET_ERROR", true);
      } else {
        const { content } = res.data;
        commit("SET_ERROR", false);
        commit("SET_LOCKERDIAGRAM", content);
      }
    },
  },
};

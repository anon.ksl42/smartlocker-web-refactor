import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import App from "./App.vue";
import { routes } from "./router/index";
import StoreData from "./store/index";
import vuetify from "./plugins/vuetify";
import { initialize } from "./util/role_access";
import VueGraph from "vue-graph";
import * as VueGoogleMaps from 'vue2-google-maps'
import { PublicFunction } from "./store/public-function/index";
import { TokenExpiry } from "./util/token_expiry";
Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueGraph);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDgxE02U8j5wEIAJKkUlT5eBCagQOxi5g0',
    libraries: 'places', // google map library u want to use
    region: 'th',
    language: 'th',
  },
  installComponents: true
});
Vue.filter("fullName", PublicFunction.fullName);
Vue.filter("getWithExpiry", TokenExpiry.getWithExpiry);
Vue.filter("shortName", PublicFunction.shortName);
Vue.filter("fomatDate", PublicFunction.fomatDate);
Vue.filter("status", PublicFunction.status);
Vue.filter("colorStatus", PublicFunction.colorStatus);
Vue.filter("statusAccount", PublicFunction.statusAccount);
Vue.filter("statusLocation", PublicFunction.statusLocation);
Vue.filter("dateNow", PublicFunction.dateNow);
Vue.filter("statusRepair", PublicFunction.statusRepair);
Vue.filter("colorStatusRepair", PublicFunction.colorStatusRepair);
const store = new Vuex.Store(StoreData);
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
initialize(store, router);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");

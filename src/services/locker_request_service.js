import { HTTPPOST, HTTPPUT, HTTPGET } from "./http_service";
const accountId = localStorage.getItem("accountId");
export default {
  isNullOrEmpty(s) {
    return s === undefined || s === "" || s === null;
  },
  addRequestForm(data) {
    return HTTPPOST("FormRequestLocker", data);
  },
  getAllFormRequestByAccountId(data) {
    return HTTPGET(
      `FormRequestLocker?AccountId=${accountId}&keyWord=${data.search}&page=${data.page}`
    );
  },
  approveForm(data) {
    return HTTPPUT("FormRequestLocker/approveform", data);
  },
  approveFormandInstall(data) {
    return HTTPPUT("FormRequestLocker/approveformandinstall", data);
  },
  getFiller(data) {
    if (this.isNullOrEmpty(data.accountId)) {
      return HTTPGET(
        `FormRequestLocker/filler?FirstName=${
          this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName.trim()
        }&LastName=${
          this.isNullOrEmpty(data.LastName) ? "" : data.LastName.trim()
        }&FormCode=${
          this.isNullOrEmpty(data.FormCode) ? "" : data.FormCode.trim()
        }&LockerCode=${
          this.isNullOrEmpty(data.LockerCode) ? "" : data.LockerCode.trim()
        }&LocateName=${
          this.isNullOrEmpty(data.LocateName) ? "" : data.LocateName.trim()
        }&SubDistrict=${
          this.isNullOrEmpty(data.SubDistrict) ? "" : data.SubDistrict.trim()
        }&PostalCode=${
          this.isNullOrEmpty(data.PostalCode) ? "" : data.PostalCode.trim()
        }&District=${
          this.isNullOrEmpty(data.District) ? "" : data.District.trim()
        }&Status=${this.isNullOrEmpty(data.Status) ? "" : data.Status}&page=${
          data.page
        }`
      );
    } else {
      return HTTPGET(
        `FormRequestLocker/filler?FirstName=${
          this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName.trim()
        }&LastName=${
          this.isNullOrEmpty(data.LastName) ? "" : data.LastName.trim()
        }&FormCode=${
          this.isNullOrEmpty(data.FormCode) ? "" : data.FormCode.trim()
        }&LockerCode=${
          this.isNullOrEmpty(data.LockerCode) ? "" : data.LockerCode.trim()
        }&LocateName=${
          this.isNullOrEmpty(data.LocateName) ? "" : data.LocateName.trim()
        }&SubDistrict=${
          this.isNullOrEmpty(data.SubDistrict) ? "" : data.SubDistrict.trim()
        }&PostalCode=${
          this.isNullOrEmpty(data.PostalCode) ? "" : data.PostalCode.trim()
        }&District=${
          this.isNullOrEmpty(data.District) ? "" : data.District.trim()
        }&Status=${this.isNullOrEmpty(data.Status) ? "" : data.Status}&page=${
          data.page
        }&accountId=${this.isNullOrEmpty(data.accountId) ? "" : data.accountId}`
      );
    }
  },
  getSize() {
    return HTTPGET("locker/lockersize");
  },
  createDiagram(data) {
    return HTTPPUT("FormRequestLocker/createDiagram", data);
  },
  getDiagram(data) {
    return HTTPGET(`locker/lockerroom/${data}`);
  },
};

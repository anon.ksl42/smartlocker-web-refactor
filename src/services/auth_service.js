import { HTTPPOST, HTTPGET,HTTPPUT } from "./http_service";

export default {
  isNullOrEmpty(s) {
    return s === undefined || s === "" || s === null;
  },
  authentication(data) {
    return HTTPPOST("account/login/manager", data);
  },
  register(data) {
    return HTTPPOST("account/register", data);
  },
  approveAccount(data) {
    return HTTPPUT("account/approve", data);
  },
  listAccount(data) {
    return HTTPGET(
      `account/partner?page=${data.Page}&FirstName=${
        this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName.trim()
      }&LastName=${
        this.isNullOrEmpty(data.LastName) ? "" : data.LastName.trim()
      }&Status=${this.isNullOrEmpty(data.Status) ? "" : data.Status}`
    );
  },
  dupEmail(data) {
    return HTTPGET(`account/emailpartner?email=${data}`);
  },
};

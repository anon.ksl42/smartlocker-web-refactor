import { HTTPGET } from "./http_service";
export default {
  isNullOrEmpty(s) {
    return s === undefined || s === "" || s === null;
  },
  exportExcelLocation(data) {
    return HTTPGET(
      `income/location/excel?FirstName=${
        this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName
      }&LastName=${
        this.isNullOrEmpty(data.LastName) ? "" : data.LastName
      }&District=${
        this.isNullOrEmpty(data.District) ? "" : data.District
      }&Province=${
        this.isNullOrEmpty(data.Province) ? "" : data.Province
      }&LocateName=${
        this.isNullOrEmpty(data.LocateName) ? "" : data.LocateName
      }&SubDistrict=${
        this.isNullOrEmpty(data.SubDistrict) ? "" : data.SubDistrict
      }&PostalCode=${
        this.isNullOrEmpty(data.PostalCode) ? "" : data.PostalCode
      }&AccountId=${data.AccountId}&DurationType=${
        data.DurationType
      }&StartDate=${data.StartDate}&EndDate=${data.EndDate}&page=${
        data.page
      }&perPage=${data.perPage}`
    );
  },
  getIncome(data) {
    return HTTPGET(
      `income/location?FirstName=${
        this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName.trim()
      }&LastName=${
        this.isNullOrEmpty(data.LastName) ? "" : data.LastName.trim()
      }&District=${
        this.isNullOrEmpty(data.District) ? "" : data.District.trim()
      }&Province=${
        this.isNullOrEmpty(data.Province) ? "" : data.Province.trim()
      }&LocateName=${
        this.isNullOrEmpty(data.LocateName) ? "" : data.LocateName.trim()
      }&SubDistrict=${
        this.isNullOrEmpty(data.SubDistrict) ? "" : data.SubDistrict.trim()
      }&PostalCode=${
        this.isNullOrEmpty(data.PostalCode) ? "" : data.PostalCode.trim()
      }&AccountId=${data.AccountId}&DurationType=${
        data.DurationType
      }&StartDate=${data.StartDate}&EndDate=${data.EndDate}&page=${
        data.page
      }&perPage=${data.perPage}`
    );
  },
  getIncomeExcelLockerRoom(data) {
    return HTTPGET(
      `income/lockerroom/excel?AccountId=${data.AccountId}&DurationType=${data.DurationType}&StartDate=${data.StartDate}&EndDate=${data.EndDate}&LockerId=${data.LockerId}&page=${data.page}&perPage=${data.perPage}`
    );
  },
  getIncomeLockerRoom(data) {
    return HTTPGET(
      `income/lockerroom?AccountId=${data.AccountId}&DurationType=${data.DurationType}&StartDate=${data.StartDate}&EndDate=${data.EndDate}&LockerId=${data.LockerId}&page=${data.page}&perPage=${data.perPage}`
    );
  },
  getIncomeExcelLocker(data) {
    return HTTPGET(
      `income/locker/excel?LockerCode=${
        this.isNullOrEmpty(data.LockerCode) ? "" : data.LockerCode
      }&AccountId=${data.AccountId}&DurationType=${
        data.DurationType
      }&StartDate=${data.StartDate}&EndDate=${data.EndDate}&LocateId=${
        data.LocateId
      }&page=${data.page}&perPage=${data.perPage}`
    );
  },
  getIncomeLocker(data) {
    return HTTPGET(
      `income/locker?LockerCode=${
        this.isNullOrEmpty(data.LockerCode) ? "" : data.LockerCode.trim()
      }&AccountId=${data.AccountId}&DurationType=${
        data.DurationType
      }&StartDate=${data.StartDate}&EndDate=${data.EndDate}&LocateId=${
        data.LocateId
      }&page=${data.page}&perPage=${data.perPage}`
    );
  },
};

import { HTTPGET, HTTPPUT, HTTPPOST } from "./http_service";
export default {
  isNullOrEmpty(s) {
    return s === undefined || s === "" || s === null;
  },
  getAllLocationById(id) {
    return HTTPGET(`locations/accountId/${id}`);
  },
  approveLocation(data) {
    return HTTPPUT(`locations/approve`, data);
  },
  editLocation(data) {
    return HTTPPOST(`locations`, data);
  },
  addLocation(data) {
    return HTTPPOST(`locations`, data);
  },
  getAllLocation(data) {
    if (this.isNullOrEmpty(data.accountId)) {
      return HTTPGET(
        `locations/filter?FirstName=${
          this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName.trim()
        }&LastName=${
          this.isNullOrEmpty(data.LastName) ? "" : data.LastName.trim()
        }&District=${
          this.isNullOrEmpty(data.District) ? "" : data.District.trim()
        }&Province=${
          this.isNullOrEmpty(data.Province) ? "" : data.Province.trim()
        }&LocateName=${
          this.isNullOrEmpty(data.LocateName) ? "" : data.LocateName.trim()
        }&SubDistrict=${
          this.isNullOrEmpty(data.SubDistrict) ? "" : data.SubDistrict.trim()
        }&PostalCode=${
          this.isNullOrEmpty(data.PostalCode) ? "" : data.PostalCode.trim()
        }&Status=${this.isNullOrEmpty(data.Status) ? "" : data.Status}&page=${
          data.page
        }&perPage=${data.perPage}`
      );
    } else {
      return HTTPGET(
        `locations/filter?FirstName=${
          this.isNullOrEmpty(data.FirstName) ? "" : data.FirstName
        }&LastName=${
          this.isNullOrEmpty(data.LastName) ? "" : data.LastName
        }&District=${
          this.isNullOrEmpty(data.District) ? "" : data.District
        }&Province=${
          this.isNullOrEmpty(data.Province) ? "" : data.Province
        }&LocateName=${
          this.isNullOrEmpty(data.LocateName) ? "" : data.LocateName
        }&SubDistrict=${
          this.isNullOrEmpty(data.SubDistrict) ? "" : data.SubDistrict
        }&PostalCode=${
          this.isNullOrEmpty(data.PostalCode) ? "" : data.PostalCode
        }&Status=${this.isNullOrEmpty(data.Status) ? "" : data.Status}&page=${
          data.page
        }&perPage=${data.perPage}&accountId=${
          this.isNullOrEmpty(data.accountId) ? "" : data.accountId
        }`
      );
    }
  },
};

import { HTTPGET } from "./http_service";
const accountId = localStorage.getItem("accountId");
export default {
  getLocateAndLocationCount() {
    return HTTPGET(`dashboard/LocateAndLocation?accountId=${accountId}`);
  },
  getIncome(rangeType) {
    return HTTPGET(
      `dashboard/Income?accountId=${accountId}&rangeGraphType=${rangeType}`
    );
  },
  getBookingLocate(data) {
    return HTTPGET(
      `dashboard/bookingLocate?accountId=${accountId}&rangeGraphType=${data.BookingLocateRangeSelected}&monthRange=${data.monthValue}`
    );
  },
  getBookingLocker(data) {
    return HTTPGET(
      `dashboard/bookingLocker?accountId=${accountId}&rangeGraphType=${data.BookingLockerRangeSelected}&monthRange=${data.monthValue}`
    );
  },
};

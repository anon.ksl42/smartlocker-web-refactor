import axios from "axios";
import axiosRetry from "axios-retry";
import { BASEURL } from "../configs/index";
import { HEADER_BEARER } from "../configs/index";

const retryDelay = (retryNumber = 0) => {
  const seconds = Math.pow(2, retryNumber) * 1000;
  const randomMs = 1000 * Math.random();
  return seconds + randomMs;
};
const _axios = axios.create({ baseURL: BASEURL });

axiosRetry(_axios, {
  retries: 20,
  retryDelay,
  // retry on Network Error & 5xx responses
  retryCondition: axiosRetry.isRetryableError,
});

export function HTTPGET(path) {
  var data = _axios.get(path, HEADER_BEARER).catch(function(error) {
    if (!error.response) {
      return Promise.reject(error);
    }else if (error.response.status == 401) {
      //remove token here  then reload
      localStorage.removeItem("user");
      localStorage.removeItem("fullName");
      localStorage.removeItem("token");
      localStorage.removeItem("accountId");
      localStorage.removeItem("type");
      localStorage.removeItem("start");
      localStorage.removeItem("end");
      return error.response.status;
    } else if (error.config && error.response && error.response.status >= 500) {
      return Promise.reject(error);
    } else {
      return error.response.status;
    }
  });
  return data;
}
export function HTTPPOST(path, formdata) {
  var data = _axios.post(path, formdata, HEADER_BEARER).catch(function(error) {
    if (!error.response) {
      return Promise.reject(error);
    }
    else if (error.response.status == 401) {
      //remove token here  then reload
      localStorage.removeItem("user");
      localStorage.removeItem("fullName");
      localStorage.removeItem("token");
      localStorage.removeItem("accountId");
      localStorage.removeItem("type");
      localStorage.removeItem("start");
      localStorage.removeItem("end");
      // location.reload();
      return error.response.status;
    } 
    else if (error.config && error.response && error.response.status >= 500) {
      return Promise.reject(error);
    } else {
      return error.response.status;
    }
  });
  return data;
}

export function HTTPPUT(path, formdata) {
  var data = _axios.put(path, formdata, HEADER_BEARER).catch(function(error) {
    if (!error.response) {
      return Promise.reject(error);
    } else if (error.response.status == 401) {
      //remove token here  then reload
      localStorage.removeItem("user");
      localStorage.removeItem("fullName");
      localStorage.removeItem("token");
      localStorage.removeItem("accountId");
      localStorage.removeItem("type");
      localStorage.removeItem("start");
      localStorage.removeItem("end");
      return error.response.status;
    } else if (error.config && error.response && error.response.status >= 500) {
      return Promise.reject(error);
    } else {
      return error.response.status;
    }
  });
  return data;
}

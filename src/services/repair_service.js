import { HTTPGET, HTTPPOST, HTTPPUT } from "./http_service";

export default {
  isNullOrEmpty(s) {
    return s === undefined || s === "" || s === null;
  },
  addRepairForm(data) {
    return HTTPPOST("RepairForm", data);
  },

  getRepairForm(filter) {
    return HTTPGET(
      `RepairForm?lockerCode=${
        this.isNullOrEmpty(filter.lockerCode) ? "" : filter.lockerCode.trim()
      }&firstName=${
        this.isNullOrEmpty(filter.firstName) ? "" : filter.firstName.trim()
      }&page=${filter.page}&accountId=${filter.accountId}&status=${
        this.isNullOrEmpty(filter.Status) ? "" : filter.Status
      }&lastName=${
        this.isNullOrEmpty(filter.lastName) ? "" : filter.lastName.trim()
      }`
    );
  },

  approveRepairForm(data) {
    return HTTPPUT(`RepairForm/approve`, data);
  },

  finishRepairForm(data) {
    return HTTPPUT(`RepairForm/finish`, data);
  },

  getAllLockerRoom(lockerId) {
    return HTTPGET(`repairform/lockerroom?lockerId=${lockerId}`);
  },
  getSummaryRepair(accountId) {
    return HTTPGET(`repairForm/summary-repair?AccountId=${accountId}`);
  },
};

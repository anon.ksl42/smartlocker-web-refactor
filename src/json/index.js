import ItemDrawer from "../json/item_drawer.json";
import ItemStatus from "../json/item_status_locker.json";
import ItemStatusLocation from "../json/item_status_location.json";
import ItemStatusAccount from "../json/item_status_account.json";
import ItemStatusRepair from "../json/item_status_repair.json";
export const index = {
  ITEM_OWNER: ItemDrawer.ItemOwner,
  ITEM_ADMIN: ItemDrawer.ItemAdmin,
  ITEM_STATUS: ItemStatus.ItemStatus,
  ITEM_STATUSLOCATION: ItemStatusLocation.ItemStatusLocation,
  ITEM_STATUSACCOUNT: ItemStatusAccount.ItemStatusAccount,
  ITEM_STATUSREAIR: ItemStatusRepair.ItemStatusRepair,
};

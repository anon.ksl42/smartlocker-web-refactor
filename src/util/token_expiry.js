
export const TokenExpiry = {
  getWithExpiry(key) {
    const itemStr = localStorage.getItem(key);
    // if the item doesn't exist, return null
    if (!itemStr) {
      return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();
    // var schedule = now.getHours() + 3;
    // compare the expiry time of the item with the current time
    // now.getTime()
    if (now.getHours() > item.expiry) {
      // If the item is expired, delete the item from storage
      // and return null
      localStorage.removeItem(key);
      localStorage.removeItem("user");
      localStorage.removeItem("fullName");
      localStorage.removeItem("accountId");
      localStorage.removeItem("type");
      localStorage.removeItem("start");
      localStorage.removeItem("end");
      return null;
    }
    return item.value;
  },
  setWithExpiry(key, value, ttl) {
    const now = new Date();
    // `item` is an object which contains the original value
    // as well as the time when it's supposed to expire

    // now.getTime()
    const item = {
      value: value,
      expiry: now.getHours() + ttl,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
};

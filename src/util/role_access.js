import { TokenExpiry } from "../util/token_expiry";
export function initialize(store, router) {
  router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const { authorize } = to.meta;
    let roleUser = store.state.auth.roleUser;
    const expiry = TokenExpiry.getWithExpiry("token");
    if (expiry == null) {
      store.dispatch("snackbar/showSnack", {
        text: "Unauthorized",
        color: "red",
        timeout: 6000,
      });
      roleUser = null;
    }
    if (to.path == "/" && roleUser) {
      return next("home");
    }
    if (authorize) {
      if (!roleUser) {
        // not logged in so redirect to login page with the return url
        return next({ name: "login" });
      }

      if (authorize.length && !authorize.includes(roleUser)) {
        // role not authorised so redirect to home page
        return next({ name: "dashboard" });
      }
    }
    return next();
  });
}
